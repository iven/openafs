#!/bin/bash
AFS_VER=$(sed -n -e '/%define \(afsvers\|pkgrel\) /p' openafs.spec | cut -d' ' -f3 | paste -sd '-' -)
KVER=${2:-latest}
# Take the repodir as a param or use .8-latest if not provided
if rpmci_is_OS8s; then
  SNAPSHOT_PATH=${3:-.s8-latest}
  echo "${SNAPSHOT_PATH}" > /etc/dnf/vars/cernstream8
elif rpmci_is_OS8al; then
  SNAPSHOT_PATH=${3:-.8-latest}
  echo "${SNAPSHOT_PATH}" > /etc/dnf/vars/cernalmalinux
elif rpmci_is_OS8el; then
  SNAPSHOT_PATH=${3:-.8-latest}
  echo "${SNAPSHOT_PATH}" > /etc/dnf/vars/cernrhel
elif rpmci_is_OS9; then
  SNAPSHOT_PATH=${3:-.s9-latest}
  echo "${SNAPSHOT_PATH}" > /etc/dnf/vars/cernstream9
elif rpmci_is_OS9al; then
  SNAPSHOT_PATH=${3:-.9-latest}
  echo "${SNAPSHOT_PATH}" > /etc/dnf/vars/cernalmalinux
elif rpmci_is_OS9el; then
  SNAPSHOT_PATH=${3:-.9-latest}
  echo "${SNAPSHOT_PATH}" > /etc/dnf/vars/cernrhel
fi

if rpmci_is_RHEL; then
  BASE_OS="baseos"
  APP_STREAM="appstream"
else
  BASE_OS="BaseOS"
  APP_STREAM="AppStream"
fi

if rpmci_is_RHEL; then
  OS="rhel"
elif rpmci_is_ALMA; then
  OS="alma"
else
  OS="centos"
fi

KVER_REPODIR="http://linuxsoft.cern.ch/cern/${OS}/${SNAPSHOT_PATH}/${BASE_OS}/x86_64/os/"

if [[ "$MAJOR" == 9 ]]; then
  KDEV_REPODIR="http://linuxsoft.cern.ch/cern/${OS}/${SNAPSHOT_PATH}/${APP_STREAM}/x86_64/os/"
else
  KDEV_REPODIR=$KVER_REPODIR
fi

echo_info "Action: $1"
echo_info "KVER: $KVER"
echo_info "SNAPSHOT_PATH: $SNAPSHOT_PATH"
echo_info "KVER_REPODIR: $KVER_REPODIR"
echo_info "KDEV_REPODIR: $KDEV_REPODIR"
echo_info "AFS_VER: $AFS_VER"

if rpmci_is_OS8; then
  yum-config-manager cr --enable
fi

if [ $1 == "srpm" ]; then
  if [ ${KVER} == "latest" ]; then
    KVER=$(repoquery --repoid baseos-kernel --repofrompath=baseos-kernel,$KVER_REPODIR --qf="%{version}-%{release}" --latest-limit 1 kernel-core)
    KERNELDEVEL="kernel-devel-${KVER}"
  else
    if rpmci_is_RHEL; then
      KERNELDEVEL="${KDEV_REPODIR}/Packages/k/kernel-devel-${KVER}.x86_64.rpm"
    else
      KERNELDEVEL="${KDEV_REPODIR}/Packages/kernel-devel-${KVER}.x86_64.rpm"
    fi
  fi

  echo_info "KERNELDEVEL: $KERNELDEVEL"

  # kernel version/release naming convention for kmod
  AFS_KVER=`echo ${KVER} | sed 's/-/_/'`

  # Given those AFS and kernel versions, what kmod should we have?
  EXPECTED_KMOD="${AFS_VER}.${AFS_KVER}${DIST}"
  echo_info "EXPECTED_KMOD: $EXPECTED_KMOD"

  # Find the latest kmod-openafs
  KMOD_VER=$(repoquery --repoid openafs-kernel --repofrompath=openafs-kernel,http://linuxsoft.cern.ch/internal/repos/$(get_kojitag)-qa/x86_64/os/ --qf="%{version}-%{release}" --latest-limit 1 kmod-openafs)
  echo_info "KMOD_VER: $KMOD_VER"

  # If we already have the kmod we need, we're done
  if [[ "$KMOD_VER" = "$EXPECTED_KMOD" ]]; then
    echo "No need to rebuild anything, the latest kmod-openafs is $KMOD_VER"
    # Store variable for cached files between CI jobs
    echo "export KMOD_AFS_PREEXISTS=true" > ./kmod_afs_exists.env
    exit 0
  fi

  # Store variable for cached files between CI jobs
  echo "export KMOD_AFS_PREEXISTS=false" > ./kmod_afs_exists.env
  yum -y install "$KERNELDEVEL"
  cp -f openafs.spec openafs-kmod-${KVER}.spec
  sed -i "2s/^/%define kernvers ${KVER}\n/" openafs-kmod-${KVER}.spec
  sed -i '0,/%define build_modules/{s/%define build_modules.*/%define build_modules 1\n%define build_userspace 0/}' openafs-kmod-${KVER}.spec
  SANITISED_KVER=`echo $KVER | sed 's/-/_/g'`
  sed -i "s/^%define pkgvers \([0-9.]\+\)/%define pkgvers \1_${SANITISED_KVER}/" openafs-kmod-${KVER}.spec
  rpmbuild -bs --define "dist ${DIST}" --define "_topdir `pwd`/build" --define "_sourcedir `pwd`/src" --define "kernvers ${KVER}" --define "build_userspace 0" --define 'build_modules 1' openafs-kmod-${KVER}.spec

elif [ $1 == "rpm" ]; then
  if [[ $KMOD_AFS_PREEXISTS = "true" ]]; then
    echo "KMOD already exists, cancelling pipeline..."
    # Add sleep so CI job output actually prints echo line
    sleep 5
    # Use API with imageci user to cancel pipeline completely
    curl --request POST --header "PRIVATE-TOKEN: $LINUXCI_APITOKEN" "https://gitlab.cern.ch/api/v4/projects/$CI_PROJECT_ID/pipelines/$CI_PIPELINE_ID/cancel"
  fi

  if [ ${KVER} == "latest" ]; then
    KVER=$(repoquery --repoid baseos-kernel --repofrompath=baseos-kernel,$KVER_REPODIR --qf="%{version}-%{release}" --latest-limit 1 kernel-core)
    KERNELDEVEL="kernel-devel-${KVER}"
  else
    if rpmci_is_RHEL; then
      KERNELDEVEL="${KDEV_REPODIR}/Packages/k/kernel-devel-${KVER}.x86_64.rpm"
    else
      KERNELDEVEL="${KDEV_REPODIR}/Packages/kernel-devel-${KVER}.x86_64.rpm"
    fi
  fi
  echo_info "KERNELDEVEL: $KERNELDEVEL"
  yum -y install "$KERNELDEVEL"

  rpm -ivh build/SRPMS/*src.rpm
  mv ~/rpmbuild/SPECS/*.spec .
  rpmci_install_builddeps

  COMMON_VER="`sed -n -e '/%define afsvers /p' openafs.spec | cut -d' ' -f3`"
  echo_info "Looking for openafs-kmod-common >= $COMMON_VER"
  COMMON=$(repoquery --repoid openafs-kernel --repofrompath=openafs-kernel,http://linuxsoft.cern.ch/internal/repos/$(get_kojitag)-qa/x86_64/os/ --qf="%{version}-%{release}" --latest-limit 1 --whatprovides "openafs-kmod-common >= $COMMON_VER")
  if [[ -z "$COMMON" ]]; then
    echo_info "openafs-kmod-common >= $COMMON_VER not found, building client first"
    make rpm || exit 1
  fi

  echo_info "Building openafs-kmod-${KVER}"
  KVER=`basename -a openafs-kmod*.spec |  sed -e 's/openafs-kmod-//' -e 's/.spec//'`
  rpmbuild -bb --define "dist ${DIST}" --define "_topdir `pwd`/build" --define "_sourcedir `pwd`/src" openafs-kmod-${KVER}.spec
fi
