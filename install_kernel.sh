#!/bin/bash
# The following script installs the kernel version corresponding to the kernel-devel version used for building the kmod-openafs package

yum install -y cern-linuxsupport-access

# Enable access for debugging
cern-linuxsupport-access enable

# set repos to testing to ensure we get the kernel via testing/cr as soon as possible. We do this as well on build_kmod.sh
if [[ "$KOJI_OS" == "8s" ]]; then
  echo "${SNAPSHOT_PATH:-.s8-latest}" > /etc/dnf/vars/cernstream8
elif [[ "$KOJI_OS" == "8al" ]]; then
  echo "${SNAPSHOT_PATH:-.8-latest}" > /etc/dnf/vars/cernalmalinux
elif [[ "$KOJI_OS" == "8el" ]]; then
  echo "${SNAPSHOT_PATH:-.8-latest}" > /etc/dnf/vars/cernrhel
elif [[ "$KOJI_OS" == "9" ]]; then
  echo "${SNAPSHOT_PATH:-.s9-latest}" > /etc/dnf/vars/cernstream9
elif [[ "$KOJI_OS" == "9al" ]]; then
  echo "${SNAPSHOT_PATH:-.9-latest}" > /etc/dnf/vars/cernalmalinux
elif [[ "$KOJI_OS" == "9el" ]]; then
  echo "${SNAPSHOT_PATH:-.9-latest}" > /etc/dnf/vars/cernrhel
fi

# also configure the repo openafsX-testing/qa to get the latest openafs version
yum-config-manager --add-repo "http://linuxsoft.cern.ch/internal/repos/${KOJI_TAG}-testing/\$basearch/os"
sed -i "s/\[.*\]/[${KOJI_TAG}-testing]\npriority=1/" /etc/yum.repos.d/linuxsoft.cern.ch*${KOJI_TAG}-testing*.repo
yum-config-manager --add-repo "http://linuxsoft.cern.ch/internal/repos/${KOJI_TAG}-qa/\$basearch/os"
sed -i "s/\[.*\]/[${KOJI_TAG}-qa]\npriority=1/" /etc/yum.repos.d/linuxsoft.cern.ch*${KOJI_TAG}-qa*.repo

# Temporary hack to be able to install older kernels
# HACKREPO="http://linuxsoft.cern.ch/internal/kernel-devel8/"
# HACKRET=`curl -s -o /dev/null --write-out '%{http_code}\n' $HACKREPO`
# if [[ "$HACKRET" -ne 404 ]]; then
#   yum-config-manager --add-repo "$HACKREPO"
# fi

# Install the kernel version used for the already built kmod package
KVER=$(rpm -qp --queryformat "%{release}\n" ~/koji/kmod-openafs*.rpm | head -n1 | cut -d'.' -f2- | sed 's/_/-/' | sed "s/$DIST//")
INSTALLED_KVER=$(uname -r | sed s/\.`arch`//)

# No need to install if version already matches the one used for building kmod
if [[ "$KVER" == "$INSTALLED_KVER" ]]; then
  echo "Installed kernel already matches the one used to build kmod, no need to reboot"
  # Add sleep so CI job output actually prints echo line
  sleep 5
  exit 0
fi

yum -y install kernel-${KVER}
if [[ $? -ne 0 ]]; then
  echo -e "\e[31mUnable to install kernel-${KVER}\e[0m"
  exit 1
fi

# We need to reboot for this kernel to come in place, as it is the latest, it will automatically selected
sudo reboot
## Warning! Rebooting the node causes ssh to return non-zero exit code (255), so we have to disable exiting shell with set +e
