#!/bin/bash
# The following script runs CERN CentOS functional tests

yum install -y git createrepo

git clone https://gitlab.cern.ch/linuxsupport/testing/cern_centos_functional_tests.git
source cern_centos_functional_tests/tests/0_lib/functions.sh

# Create a repo for the generated RPMs from the previous step
createrepo -v /root/koji/
cat > /etc/yum.repos.d/openafs-kernel.repo  <<DELIM
[openafs-kernel]
name=openafs-kernel
baseurl=file:///root/koji
enabled=1
priority=1
gpgcheck=0
DELIM

# Install generated rpm
echo "Installing kmod-openafs"
dnf install -y kmod-openafs
RET=$?
if [[ $RET -eq 0 ]]; then
    t_Log "PASS"
else
    t_Log "FAIL"
    exit $RET
fi

cd cern_centos_functional_tests

# Override test skipping. we want to run openafs only which we disable by default
cat > ./skipped-tests.list  <<DELIM
# This file contains list of tests we need/want to skip
# Reason is when there is upstream bug that we're aware of
# So this file should contain:
#  - centos version (using $centos_ver)
#  - test to skip (tests/p_${name}/test.sh)
#  - reason why it's actually skipped (url to upstream BZ, or bug report)
# Separated by |
8|tests/0_common/00_centos_repos.sh|We want to keep CERN repos enabled
9|tests/0_common/00_centos_repos.sh|We want to keep CERN repos enabled
8|tests/0_common/20_upgrade_all.sh|Avoid too much noise on CI logs
9|tests/0_common/20_upgrade_all.sh|Avoid too much noise on CI logs
8|tests/0_common/30_dns_works.sh|We spawn VM without waiting for DNS register
9|tests/0_common/30_dns_works.sh|We spawn VM without waiting for DNS register
DELIM


# Run afs tests specifically
./runtests.sh cern_openafs
