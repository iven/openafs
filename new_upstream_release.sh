#!/bin/bash
## helper script to update this repo to a new upstream release

version="$1"
if [[ -z "$version" || "$version" != 1.?.* ]]; then
	echo -e "usage: $0 1.x.y\n\nremove previous version from this tree, download new version 1.x.y, fix hardcoded strings\nReview result, then add to git repo."
	exit 1
fi

set -e
oldversion=$( sed -n -e '/%define afsvers /{s/%define afsvers *//;p}' openafs.spec )

# remove old, download new files
git rm src/{openafs-"${oldversion}"-src.tar.bz2,openafs-"${oldversion}"-doc.tar.bz2,RELNOTES-"${oldversion}"} ||:
rm src/ChangeLog ||:  # nomal 'rm', we want to keep file history
# pre-releases live elsewhere
candidate=""
if [[ "$version" =~ pre ]]; then
  candidate="candidate/"
fi
wget -c -P src/ https://www.openafs.org/dl/openafs/"${candidate}""${version}"/{openafs-"${version}"-src.tar.bz2,openafs-"${version}"-doc.tar.bz2,RELNOTES-"${version}",ChangeLog}
git add src/{openafs-"${version}"-src.tar.bz2,openafs-"${version}"-doc.tar.bz2,RELNOTES-"${version}",ChangeLog}

# fixup hardcoded values. We restart the release at -0 every time
release=0
sed -i -e "/^%define \(afsvers\|pkgvers\)/s/${oldversion}/${version}/" -e "/^%define pkgrel/s/^%define pkgrel .*/%define pkgrel ${release}/" openafs.spec

changelogline="* $(date '+%a %b %d %Y') $(git config --get user.name) <$(git config --get user.email)> - ${version}-${release}"
sed -i -e "/^%changelog/a ${changelogline}\n- update to upstream openafs-${version}\n" openafs.spec

git add openafs.spec
echo "Please review pending changes, manually 'git commit' if OK".
